import {MigrationInterface, QueryRunner} from "typeorm";

export class createTables1646183642889 implements MigrationInterface {
    name = 'createTables1646183642889'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "emailCode" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "code" character varying NOT NULL, "generatedDate" TIMESTAMP NOT NULL DEFAULT '2022-03-02T01:14:07.282Z', "userId" uuid, CONSTRAINT "REL_8a435e28861d030872a89b7255" UNIQUE ("userId"), CONSTRAINT "PK_3899a9a19fbb020f13d87c98ecb" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "emailCode" ADD CONSTRAINT "FK_8a435e28861d030872a89b72556" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "emailCode" DROP CONSTRAINT "FK_8a435e28861d030872a89b72556"`);
        await queryRunner.query(`DROP TABLE "emailCode"`);
    }

}
