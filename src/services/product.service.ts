import { getCustomRepository } from "typeorm";
import ProductRepository from "../repositories/productRepository";
import AppError from "../errors/AppError";


interface ProductBody {
    name: string,
    description?: string,
    price: number,
    amount: number
}


export const createProduct = async (body: ProductBody) => {
    const { name, description, price, amount } = body;
    try {

        const productRepository = getCustomRepository(ProductRepository);
        
        const product = productRepository.create({
            name, 
            description,
            price,
            amount
        });
        
        await productRepository.save(product);
        
        return product;

    } catch (error) {
        throw new AppError((error as any).message, 400);
    }
}

export const findProduct = async (id: string | undefined) => {
    const productRepository = getCustomRepository(ProductRepository);

    const product = await productRepository.findOne(id);
    
    return product;
}

export const listProduct = async (page = 1) => {
    const productRepository = getCustomRepository(ProductRepository);

    const products = await productRepository.findPaginated(page);

    return products;
}
