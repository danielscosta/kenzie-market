import { getCustomRepository } from "typeorm";
import { Cart, User } from "../entities";
import AppError from "../errors/AppError";
import CartRepository from "../repositories/cartRepository";
import { findProduct } from "./product.service";
import { findUser } from "./user.service";

export const create = async (user: User | undefined) => {
    try {
        const cartRepository = getCustomRepository(CartRepository);

        if (!user) {
            throw new AppError('User undefined', 400);
        }
    
        const cart = cartRepository.create({
            user: user,
            total: 0
        });

        await cartRepository.save(cart);
        
        return cart;

    } catch (error) {
        throw new AppError((error as any).message, 400);
    }
        
}

export const findCart = async (user: User | undefined) => {
    try {
        const cartRepository = getCustomRepository(CartRepository);

        if (!user) {
            throw new AppError('User undefined', 400);
        }

        let cart = await cartRepository.findOne({
            where: {
                user: user
            },
            relations: ['products']
        });

        if (!cart && user) {
            cart = await create(user);
        }
        
        return cart;
    } catch (error) {
        throw new AppError((error as any).message, 400);
    }
}


export const addProduct = async (productId: string, user: User | undefined ) => {
    try {
        const cartRepository = getCustomRepository(CartRepository);

        const productAdd = await findProduct(productId);

        const cart = await findCart(user)

        if (cart && productAdd){
            
            const productExists = cart.products.find(product => product.id === productAdd.id);

            if (productExists){
                throw new AppError('Existing product in cart!', 400);
            }
            cart.products = [...cart.products, productAdd];
            cart.total += productAdd.price;
            await cartRepository.save(cart);
        }
        
        return cart;

    } catch (error) {
        throw new AppError((error as any).message, 400);
    }
}


export const listCart = async (page = 1) => {
    try {
        const cartRepository = getCustomRepository(CartRepository);

        const carts = await cartRepository.findPaginated(page);

        return carts;
    } catch (error) {
        throw new AppError((error as any).message, 400);
    }
}

export const deleteProduct = async (product_id: string, user: User | undefined) => {
    try {
        const cartRepository = getCustomRepository(CartRepository);
        const productDelete = await findProduct(product_id);
        const cart = await findCart(user);

        
        if(cart && productDelete) {
            const productExists = cart.products.find(product => product.id === productDelete.id);
            if (!productExists){
                throw new AppError('Product is not in cart!', 400);
            }
            cart.products = cart.products.filter(product => product.id !== productDelete.id);
            cart.total -= productDelete.price;
            await cartRepository.save(cart);
        }

        return cart;
    } catch (error) {
        throw new AppError((error as any).message, 400);
    }
}

export const cleanCart = async (cart: Cart) => {

    const cartRepository = getCustomRepository(CartRepository);
    cart.products = [];
    cart.total = 0;
    await cartRepository.save(cart);
} 
