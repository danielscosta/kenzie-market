import { getCustomRepository } from "typeorm";
import { Cart, User } from "../entities";
import AppError from "../errors/AppError";
import BuyRepository from "../repositories/buyRepository";
import { cleanCart } from "./cart.service";


export const closeCart = async (user: User | undefined, cart: Cart | undefined) => {
    try {
        const buyRepository = getCustomRepository(BuyRepository);

        if(!cart || !cart.products.length) {
            throw new AppError('Empty cart', 400);
        }

        const buy = buyRepository.create({
            user: user,
            total: cart.total,
            products: cart.products
        });

        await buyRepository.save(buy);

        cleanCart(cart);

        return buy;

    } catch (error) {
        throw new AppError((error as any).message, 400);
    }
}

export const findBuy = async (id: string) => {
    try {
        const buyRepository = getCustomRepository(BuyRepository);

        const buy = await buyRepository.findOne({
            where: {
                id: id
            },
            relations: ['products']
        })

        return buy;
    } catch (error) {
        throw new AppError((error as any).message, 400);
    }
}

export const listBuys = async (page = 1) => {
    try {
        const buyRepository = getCustomRepository(BuyRepository);

        const carts = await buyRepository.findPaginated(page);

        return carts;
    } catch (error) {
        throw new AppError((error as any).message, 400);
    }
}
