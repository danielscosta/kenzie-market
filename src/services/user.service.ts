import { getCustomRepository, ObjectID } from "typeorm";
import UserRepository from "../repositories/userRepository";
import AppError from "../errors/AppError";
import bcrypt from 'bcrypt';

interface UserBody {
    name: string,
    email: string,
    password: string,
    isAdm: boolean
}


export const createUser = async (body: UserBody) => {
    const { name, email, password, isAdm } = body;
    try {

        const userRepository = getCustomRepository(UserRepository);

        const user = userRepository.create({
            name,
            email,
            password,
            isAdm
        });

        await userRepository.save(user);

        return user;

    } catch (error) {
        throw new AppError((error as any).message, 400);
    }
}

export const findUser = async (id: string | undefined) => {
    const userRepository = getCustomRepository(UserRepository);

    const user = await userRepository.findOne(id);

    return user;
}

export const findUserEmail = async (email: string | undefined) => {
    const userRepository = getCustomRepository(UserRepository);

    const user = await userRepository.findOne({ email: email });

    return user;
}

export const listUser = async (page = 1) => {
    const userRepository = getCustomRepository(UserRepository);

    const users = await userRepository.findPaginated(page);

    return users;
}

export const updatePassword = async (password: string, email: string) => {
    const userRepo = getCustomRepository(UserRepository);
    const user = await userRepo.findOne({ email: email });
    if (user) {
        await userRepo.update(user.id, {
            password: bcrypt.hashSync(password, 10),
        });
    }
};

