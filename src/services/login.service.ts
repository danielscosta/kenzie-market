import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { getCustomRepository } from "typeorm"
import AppError from '../errors/AppError';
import UserRepository from "../repositories/userRepository"

export const authenticateUser = async (email: string, password: string) => {
    const userRepository = getCustomRepository(UserRepository);

    const user = await userRepository.findByEmail(email);
    
    if (user === undefined || !bcrypt.compareSync(password, user.password)) {
        throw new AppError('Wrong email/password', 401);
    }
    
    const token = jwt.sign({ id: user.id },  'secret', { expiresIn: '1d' });

    return token;
}
