import { EntityRepository, Repository } from "typeorm";
import { Cart } from "../entities";


@EntityRepository(Cart)
class CartRepository extends Repository<Cart> {
    public async findPaginated(page = 1): Promise<Cart[] | undefined> {
        const carts = await this.find({
            order: {
                user: 'ASC'
            },
            relations: ['products'],
            take: 5,
            skip: (page - 1) * 5
        });

        return carts;
    }
    
}

export default CartRepository;