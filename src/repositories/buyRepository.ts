import { EntityRepository, Repository } from "typeorm";
import { Buy } from "../entities";


@EntityRepository(Buy)
class BuyRepository extends Repository<Buy> {
    public async findPaginated(page = 1): Promise<Buy[] | undefined> {
        const buys = await this.find({
            order: {
                createdAt: 'ASC'
            },
            relations: ['products'],
            take: 5,
            skip: (page - 1) * 5
        });

        return buys;
    }
}

export default BuyRepository;