import { EntityRepository, Repository } from "typeorm";
import { EmailCode } from "../entities/emailCode";

@EntityRepository(EmailCode)
export default class emailRepository extends Repository<EmailCode> { }
