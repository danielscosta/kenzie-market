import { EntityRepository, Repository } from "typeorm";
import { Product } from "../entities";


@EntityRepository(Product)
class ProductRepository extends Repository<Product> {
    public async findPaginated(page = 1): Promise<Product[] | undefined> {
        const products = await this.find({
            order: {
                name: 'ASC'
            },
            take: 5,
            skip: (page - 1) * 5
        });

        return products;
    }
    
}

export default ProductRepository;