import { Request, Response, NextFunction } from "express";
import { closeCart, findBuy, listBuys } from "../services/buy.service";
import { findCart } from "../services/cart.service";
import { EmailServices } from "../services/email.service";
import { findUser } from "../services/user.service";
import { EmailController } from "./email.controller";

export const buy = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user = await findUser(req.user?.id);
        const cart = await findCart(user);

        const buy = await closeCart(user, cart);

        EmailController.emailBuy(buy.id, user?.email);

        res.send(buy);

    } catch (error) {
        next(error);
    }
}

export const getBuy = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { id } = req.params;
        const buy = await findBuy(id);

        res.send(buy);

    } catch (error) {
        next(error);
    }
}

export const list = async (req: Request, res: Response, next: NextFunction) => {

    try {
        const { page } = req.query;
        if (typeof page === "string") {
            const buy = await listBuys(parseInt(page));

            res.send(buy);
        } else {
            const buy = await listBuys();

            res.send(buy);
        }

    } catch (error) {
        next(error);
    }

}