import { Request, Response, NextFunction } from "express";
import AppError from "../errors/AppError";
import { createUser, findUser, listUser } from "../services/user.service";

export const create = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user = await createUser(req.body);

        res.send({
            'id': user.id,
            'name': user.name,
            'email': user.email,
            'isAdm': user.isAdm,
            'createdOn': user.createdOn,
            'updatedOn': user.updatedOn
        });
    } catch (error) {
        next(error);
    }

}

export const findUserId = async (req: Request, res: Response, next: NextFunction) => {
    try {  
        const { id } = req.params;

        const user = await findUser(id);

        res.send(user);
    } catch (error) {
        next( new AppError('User not found!', 404));     
    }
}

export const list = async (req: Request, res: Response) => {
    const pageQuery = req.query.page ? parseInt(req.query.page as string) : 1;
    const users = await listUser(pageQuery);

    res.send(users);
}


