import { NextFunction, Request, Response } from "express";
import AppError from "../errors/AppError";
import { findBuy } from "../services/buy.service";
import { EmailServices } from "../services/email.service";
import { RecoverServices } from "../services/recover.service";
import { findUserEmail, updatePassword } from "../services/user.service";
// import { RetrieveServices } from "../services/retrieveServices";
// import { ApiError } from "../utils/errors";
// import { UserServices } from "../services/userServices";

export class EmailController {
    static postEmailRecover = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const email = req.body.email;
            const codeEmail = await RecoverServices.searchCode(email);
            if (codeEmail) {
                const emailOptions = {
                    from: process.env.MAILER_USER,
                    to: email,
                    template: "recover",

                    context: {
                        code: codeEmail.code,
                    },
                };

                const transport = EmailServices.transport();
                transport.sendMail(emailOptions);
                return res.status(200).json({ message: "E-mail sent" });
            }
        } catch (err) {
            console.log(err);
            next(err);
        }
    };

    static emailBuy = async (idBuy: string, email: string | undefined) => {
        const buy = await findBuy(idBuy);

        const emailOptions = {
            from: process.env.EMAIL,
            to: email,
            subject: "Compra efetuada",
            template: "buy",
            context: {
                total: buy?.total
            },
        };

        const transport = EmailServices.transport();
        transport.sendMail(emailOptions);
    }

    static sendEmail = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const { email, message } = req.body;
            const user = await findUserEmail(email);
            if (!user) {
                throw new AppError("User not found", 404);
            }
            const emailOptions = {
                from: process.env.MAILER_USER,
                to: email,
                subject: "Kenzie Market Contact",
                html: `<p>${message}</p>`
            };

            const transport = EmailServices.transport();
            transport.sendMail(emailOptions);
            return res.status(200).json({ message: "E-mail sent" });
        } catch (err) {
            next(err);
        }
    };

    static changePasswordRoute = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const data = req.body;
            const code = await RecoverServices.getCode(data.email);
            if (!code) {
                return new AppError("Invalid Email", 422);
            }
            if (data.code != code.code) {
                return new AppError("Invalid Code", 422);
            }
            await updatePassword(data.newPassword, data.email);
            return res.sendStatus(204);
        } catch (e) {
            next(e);
        }
    };
}
