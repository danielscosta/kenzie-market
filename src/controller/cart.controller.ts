import { Request, Response, NextFunction } from "express";
import AppError from "../errors/AppError";
import { create, addProduct, listCart, findCart, deleteProduct } from "../services/cart.service";
import { findUser } from "../services/user.service";

export const addProductCart = async (req: Request, res: Response, next: NextFunction) => {
    try {  
        const user = await findUser(req.user?.id)
        const { productId } = req.body
        let cart = await findCart(user);

        if (!cart) {
            cart = await create(user);
        }

        cart = await addProduct(productId, user)

        res.send(cart);
    } catch (error) {
        next(error);     
    }
}

export const getCart = async (req: Request, res: Response, next: NextFunction ) => {

    try {
        const { id } = req.params;
        const user = await findUser(id)
        const cart = await findCart(user);

        res.send(cart);
    } catch (error) {
        next(error);
    }

}

export const list = async (req: Request, res: Response, next: NextFunction ) => {

    try {
        const cart = await listCart();

        res.send(cart);
    } catch (error) {
        next(error);
    }

}

export const remove = async (req: Request, res: Response, next: NextFunction ) => {
    try {
        
        const { product_id } = req.params;
        const user = await findUser(req.user?.id);

        const cart = await deleteProduct(product_id, user);

        res.send(cart);
    } catch (error) {
        next(error);
    }
}