import { Request, Response, NextFunction } from "express";
import { createProduct, findProduct, listProduct } from "../services/product.service";
import AppError from "../errors/AppError";


export const create = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const product = await createProduct(req.body);

        res.send(product);
    } catch (error) {
        next(error);
    }

}

export const findProductId = async (req: Request, res: Response, next: NextFunction) => {
    try {  
        const { id } = req.params;

        const product = await findProduct(id);

        res.send(product);
    } catch (error) {
        next( new AppError('Product not found!', 404));     
    }
}

export const list = async (req: Request, res: Response) => {
    const pageQuery = req.query.page ? parseInt(req.query.page as string) : 1;
    const products = await listProduct(pageQuery);

    res.send(products);
}