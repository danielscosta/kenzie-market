import { Router } from "express";
import { validate } from '../middlewares/validation.middleware';
import emailSchema from "../schemas/emailSchema";
import { checkIsAdm } from "../middlewares/isAdm.middleware";
import { EmailController } from "../controller/email.controller";

const emailRouter = () => {
    const router = Router();

    router.post("", validate(emailSchema), checkIsAdm, EmailController.sendEmail);

    return router;
};

export default emailRouter;
