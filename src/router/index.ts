import { Express } from "express";
import { buyRouter } from "./buy.router";
import { cartRouter } from "./cart.router";
import emailRouter from "./email.router";
import { loginRouter } from "./login.router";
import { productRouter } from "./product.router";
import recoverRouter from "./recoverRoutes";
import { userRouter } from "./user.router"


export const initializerRouter = (app: Express) => {
    app.use('/login', loginRouter());
    app.use('/user', userRouter());
    app.use('/product', productRouter());
    app.use('/cart', cartRouter());
    app.use('/buy', buyRouter());
    app.use('/email', emailRouter());
    app.use('', recoverRouter())
}


