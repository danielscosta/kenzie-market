import { Router } from "express";
import { EmailController } from "../controller/email.controller";
import { validate } from "../middlewares/validation.middleware";
import changePasswordSchema from "../schemas/changePasswordSchema";
import recoverSchema from "../schemas/recoverSchema";

const recoverRouter = () => {
    const router = Router();

    router.post("/recuperar", validate(recoverSchema), EmailController.postEmailRecover);

    router.post("/alterar_senha", validate(changePasswordSchema), EmailController.changePasswordRoute);

    return router;
};

export default recoverRouter;
