import { Router } from "express";
import { buy, list, getBuy } from "../controller/buy.controller";
import { isAuthenticated } from "../middlewares/authentication.middleware";
import { checkIsAdm } from "../middlewares/isAdm.middleware";

const router = Router();

export const buyRouter = () => {
    router.post('', isAuthenticated, buy);
    router.get('/:id', isAuthenticated, getBuy);
    router.get('', isAuthenticated, checkIsAdm, list);

    return router;
}