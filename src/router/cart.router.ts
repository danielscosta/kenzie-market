import { Router } from "express";
import { isAuthenticated } from "../middlewares/authentication.middleware";
import { checkIsAdm } from "../middlewares/isAdm.middleware";
import { addProductCart, list, getCart, remove } from "../controller/cart.controller";


const router = Router();

export const cartRouter = () => {
    router.post('', isAuthenticated, addProductCart);
    router.get('/:id', isAuthenticated, checkIsAdm, getCart);
    router.get('', isAuthenticated, checkIsAdm, list);
    router.delete('/:product_id', isAuthenticated, checkIsAdm, remove)

    return router;
}