import { Router } from "express";

import { create, findUserId, list } from "../controller/user.controller";
import { validate } from '../middlewares/validation.middleware';
import { UserSchema } from "../schemas/userSchema";
import { isAuthenticated } from "../middlewares/authentication.middleware";
import { checkIsAdm } from "../middlewares/isAdm.middleware";


const router = Router();

export const userRouter = () => {
    router.post('', validate(UserSchema), create);
    router.get('/:id', isAuthenticated, checkIsAdm, findUserId);
    router.get('', isAuthenticated, checkIsAdm, list);

    return router;
}
