import { Router } from "express";
import { create, findProductId, list } from "../controller/product.controller";
import { checkIsAdm } from "../middlewares/isAdm.middleware";
import { validate } from "../middlewares/validation.middleware";
import { ProductSchema } from "../schemas/productSchema";

const router = Router();

export const productRouter = () => {
    router.post('', validate(ProductSchema), checkIsAdm, create);
    router.get('/:id', findProductId)
    router.get('', list);

    return router;
}


