import {
    PrimaryGeneratedColumn,
    CreateDateColumn,
    Entity,
    Column,
    OneToOne,
    JoinColumn,
} from "typeorm";
import { User } from "./User";

@Entity("emailCode")
export class EmailCode {
    @PrimaryGeneratedColumn("uuid")
    id!: string;

    @Column("varchar")
    code!: string;

    @CreateDateColumn({ default: new Date().toISOString() })
    generatedDate!: Date;

    @OneToOne(() => User)
    @JoinColumn()
    user!: User;
}
