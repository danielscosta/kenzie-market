import { Entity, PrimaryGeneratedColumn, Column } from "typeorm"; 

@Entity('products')
export class Product {
    @PrimaryGeneratedColumn('uuid')
    id!: string;
    
    @Column({ unique: true })
    name: string;

    @Column()
    description: string;

    @Column({ type: "float" })
    price: number;

    @Column({ type: "int" })
    amount: number;

    constructor(name: string, description: string = "", price: number, amount: number = 1) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.amount = amount;
    }

}