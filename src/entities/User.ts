import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, BeforeInsert, OneToMany } from 'typeorm';
import bcrypt from 'bcrypt';

import { Buy } from './Buy';

@Entity('users')
export class User {
    @PrimaryGeneratedColumn('uuid')
    id!: string;

    @Column()
    name: string;

    @Column({ unique: true })
    email: string;

    @Column()
    password: string;

    @Column()
    isAdm: boolean;

    @CreateDateColumn()
    createdOn!: Date;

    @UpdateDateColumn()
    updatedOn!: Date;

    @BeforeInsert()
    hashPassword() {
        this.password = bcrypt.hashSync(this.password, 10);
    }

    @OneToMany(() => Buy, buy => buy.user)
    buys!: Buy[];


    constructor(email: string, password: string, name: string, isAdm: boolean) {
        this.name = name;
        this.email = email;
        this.password = password,
            this.isAdm = isAdm
    }

    toJSON() {
        const { password, ...rest } = this;
        return rest;
    }

}
