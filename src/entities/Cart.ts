import { Entity, PrimaryGeneratedColumn, OneToOne, JoinColumn, JoinTable, ManyToMany, Column } from "typeorm"; 

import { User } from "./User";
import { Product } from "./Procuct";

@Entity('cart') 
export class Cart { 

    @PrimaryGeneratedColumn('uuid')
    id!: string;

    @Column({ type: "float" })
    total!: number;

    @OneToOne(type => User) @JoinColumn() 
    user!: User;

    @ManyToMany(type => Product) @JoinTable()
    products!: Product[];

}