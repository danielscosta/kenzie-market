import { Entity, PrimaryGeneratedColumn, JoinTable, ManyToMany, Column, CreateDateColumn, ManyToOne } from "typeorm"; 

import { User } from "./User";
import { Product } from "./Procuct";

@Entity('buys') 
export class Buy { 

    @PrimaryGeneratedColumn('uuid')
    id!: string;

    @Column({ type: "float" })
    total!: number;

    @CreateDateColumn()
    createdAt!: Date;

    @ManyToOne(() => User, user => user.buys)  
    user!: User;

    @ManyToMany(type => Product) @JoinTable()
    products!: Product[];

}