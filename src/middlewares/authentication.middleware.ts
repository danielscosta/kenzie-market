import jwt from 'jsonwebtoken';
import { Request, Response, NextFunction } from 'express';

import AppError from '../errors/AppError';

export const isAuthenticated = (req: Request, res: Response, next: NextFunction) => {
    const token = req.headers.authorization?.split(' ')[1];

    jwt.verify(token as string, 'secret' as string, (err: any, decoded: any) => {
        if (err) {
            return next(new AppError('Missing authorization headers', 401));
        }
        
        const userId = decoded.id;
        
        req.user = { id: userId };

        next();
    });
}
