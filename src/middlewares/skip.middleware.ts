import { Request, Response, NextFunction } from "express";

export const setSkipMiddleware = (req: Request, res: Response, next: NextFunction) => {
    req.skip = true;

    next();
}

export const canSkipMiddleware = (req: Request, res: Response, next: NextFunction) => {
    if (req.skip) {
        return next();
    }

    res.send('Not skiped');
}
