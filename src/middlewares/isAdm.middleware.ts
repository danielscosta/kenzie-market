import { Request, Response, NextFunction } from 'express';
import { findUser } from '../services/user.service';

import AppError from '../errors/AppError';

export const checkIsAdm = async (req: Request, res: Response, next: NextFunction) => {
    const userId = req.user?.id;

    const { id } = req.params;

    const user = await findUser(userId);
    
    const isAdm = user?.isAdm;

    if (!isAdm && !id ) {
        return next(new AppError('Unauthorized', 401));
    }
    if (id && id !== userId && !isAdm) {
        return next(new AppError('Missing admin permissions', 401));
    }

    next();
}
