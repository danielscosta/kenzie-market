import * as yup from "yup";

const recoverSchema = yup.object().shape({
    email: yup.string().required().email()
});

export default recoverSchema;
