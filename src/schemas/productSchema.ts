import * as yup from 'yup';

export const ProductSchema = yup.object().shape({
    name: yup.string().required(),
    description: yup.string(),
    price: yup.number().required(),
    amount: yup.number()
});
