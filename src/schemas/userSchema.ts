import * as yup from 'yup';

export const UserSchema = yup.object().shape({
    email: yup.string().required().email(),
    name: yup.string().required(),
    password: yup.string().required().min(6),
    isAdm: yup.boolean().required()
});
