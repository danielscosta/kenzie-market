const devEnv = {
    type: 'postgres',
    host: 'localhost',
    port: '5432',
    database: 'entrega6',
    username: 'postgres',
    password: '123456',
    entities: ['./src/entities/**/*.ts'],
    migrations: ['./src/database/migrations/*.ts'],
    cli: {
        migrationsDir: './src/database/migrations'
    },
    logging: true,
    synchronize: false,
};


module.exports = devEnv;